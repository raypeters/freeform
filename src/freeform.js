;( function( root, factory ) {
  if ( typeof define === 'function' && define.amd ) {
    define( [], factory );
  } else if ( typeof exports === 'object' ) {
    module.exports = factory( root );
  } else {
    root.Freeform = factory( root );
  }
}( this, function( scope ) {

  // @note resets the options
  Freeform.prototype.reset = reset;

  // @note builds the options and updates current branch
  Freeform.prototype.update = update;

  // @note a cascading method that attempts to resolve all available options
  Freeform.prototype.resolve = resolve;

  // @note submit the form
  Freeform.prototype.submit = submit;

  // @note returns the current flowSteps
  Freeform.prototype.flowSteps = flowSteps;

  return Freeform;

  function Freeform( formId, settings, selected ) {
    // @note cart builder instance settings
    this.settings = settings;

    // @note the user selected options (optional)
    this.options = ( selected || {} );

    // @note bind to an element
    this.element = document.getElementById( formId );

    return _activate( this );
  }

  function _activate( vm ) {
    _syncFormElements( vm );

    return vm;
  }

  function _syncFormElements( vm ) {
    var settingKeys;

    if ( vm.element ) {
      vm.element.Freeform = vm;
      vm.element.classList += ' ready';

      settingKeys = vm.element.querySelectorAll( '[data-ff-key]' );

      _each( settingKeys, function( elem ) {
        elem.removeEventListener( 'click', _onKeyElementClick, false );
        elem.addEventListener( 'click', _onKeyElementClick, false );
        elem.style.display = 'none';
      } );
    }

    vm.resolve();

    return;

    function _onKeyElementClick( ev ) {
      _findAndSyncDatasetAttributes( vm, this, ev );
    }
  }


  function _findAndSyncDatasetAttributes( vm, keyElement, ev ) {
    var node, attrs, previouslySelected;

    node = ev.target;

    previouslySelected = keyElement.querySelector( '.selected' );

    if ( previouslySelected ) {
      previouslySelected.classList = String( previouslySelected.classList ).replace( 'selected', '' );
    }

    while ( node !== keyElement ) {
      if ( node.dataset.ffVal ) {
        node.classList += ' selected';

        attrs = {};
        attrs[ keyElement.dataset.ffKey ] = node.dataset.ffVal;

        vm.update( attrs );

        return;
      }

      node = node.parentNode;
    }

    // @note no ffVal found
  }

  function reset() {
    this.options = {};
    _syncFormElements( this );

    return this;
  }

  function update( attrs ) {
    this.options = _extend( this.options, attrs );
    this.resolve();

    return this;
  }

  /**
   * resolve
   *
   * Resolves a flow key. Cascades into child flows.
   *
   */
  function resolve( options ) {
    var vm, keys, flowSteps;

    vm = this;

    flowSteps = vm.flowSteps();

    _each( flowSteps, function( step ) {
      _resolve( step, vm.options );
    } );

    keys = Object.keys( vm.options );

    console.log( vm.options );

    // @note done check
    if ( flowSteps.length === keys.length ) {
      return _done();
    }

    // @note sync which flowsteps are displayed
    _syncFlowStepKeyElements();

    return;

    function _syncFlowStepKeyElements() {
      var flowStep, activeFlowSteps, firstInactiveFlowStep, i, l;

      activeFlowSteps = [];

      i = 0;
      l = flowSteps.length;

      for ( ; i < l; ++i ) {
        flowStep = flowSteps[ i ];

        if ( vm.options[ flowStep.key ] !== undefined ) {
          _showFlowStep( flowStep );
          continue;
        }

        if ( ! firstInactiveFlowStep ) {
          _showFlowStep( flowStep );
          firstInactiveFlowStep = true;
        }
      }
    }

    function _showFlowStep( flowStep ) {
      var flowKeyElement;

      flowKeyElement = vm.element.querySelector( '[data-ff-key="' + flowStep.key + '"]' );

      if ( flowKeyElement.style.display === 'none' ) {
        setTimeout( function() {
          flowKeyElement.scrollIntoView( true );
        }, 10 );
      }

      flowKeyElement.style.display = 'block';
    }

    function _isActive( item ) {
      return item.active;
    }

    function _done() {
      vm.element.classList += ' done';
      _prepareSubmit( vm );
    }
  }

  function _resolve( flowStep, options ) {
    var val;

    if ( flowStep.resolve ) {
      _each( flowStep.flow, _resolve );

      val = flowStep.resolve( options );

      if ( val ) {
        options[ flowStep.key ] = val;
      }
    }

    if ( options[ flowStep.key ] !== undefined ) {
      flowStep.active = true;
    }
  }

  function _transformResponse( vm, response ) {
    var mapping;

    if ( ! vm.settings.transformer ) {
      return response;
    }

    mapping = vm.settings.transformer( vm.options );

    if ( ! mapping ) {
      return response;
    }

    return _mapResponse();

    function _mapResponse() {
      var key, options;

      for ( key in response ) {
        options = mapping[ key ];

        if ( ! options ) {
          continue;
        }

        response[ key ] = _getVariantLike( response[ key ], options );
      }

      return response;
    }
  }

  function _getVariantLike( keyword, variants ) {
    var i, l;

    if ( typeof variants === 'string' ) {
      return variants;
    }

    i = 0;
    l = variants.length;

    for ( ; i < l; ++i ) {
      if ( variants[ i ].indexOf( keyword ) !== -1 ) {
        return variants[ i ];
      }
    }
  }

  function submit() {
    _prepareSubmit( this );
    document.location = this.action;
  }

  function _prepareSubmit( vm ) {
    var response;

    response = _extend( {}, vm.options );

    _each( vm.settings.flow, function( step ) {
      _resolve( step, response );
    } );

    response = _transformResponse( vm, response );

    vm.action = vm.settings.action( response );
  }

  function flowSteps() {
    return _getFlowSteps( this, this.settings.flow );
  }


  function _getFlowSteps( vm, flow ) {
    var ret, flowStep, i, l;

    ret = [];

    i = 0;
    l = flow.length;

    for ( ; i < l; ++i ) {
      flowStep = flow[ i ];

      if (
        typeof flowStep.require === 'function' &&
        ! flowStep.require( vm.options )
      ) {
        continue;
      }

      if ( flowStep.flow ) {
        ret = ret.concat( flowStep.flow );
        continue;
      }

      ret.push( flowStep );
    }

    return ret;
  }

  function _find( iterable, fn ) {
    var i, l;

    i = 0;
    l = iterable.length;

    for ( ; i < l; ++i ) {
      if ( fn( iterable[ i ] ) ) {
        return iterable[ i ];
      }
    }

    return null;
  }

  function _filter( iterable, fn ) {
    var ret;

    ret = [];

    _each( iterable, function( item ) {
      if ( fn( item ) ) {
        ret.push( item );
      }
    } );

    return ret;
  }

  function _every( iterable, callback ) {
    var key;

    for ( key in iterable ) {
      callback( iterable[ key ], key, iterable );
    }
  }

  function _each( iterable, callback ) {
    var i, l;

    i = 0;
    l = iterable.length;

    for ( ; i < l; ++i ) {
      if ( callback( iterable[ i ], i, iterable ) ) {
        break;
      }
    }
  }

  function _extend( target, source ) {
    var prop;

    target = ( target || {} );

    for ( prop in source ) {
      if ( source.hasOwnProperty( prop ) ) {
        if ( typeof source[ prop ] === 'object' ) {
          if ( source[ prop ] instanceof Date ) {
            target[ prop ] = new Date( source[ prop ] );
          } else if ( Array.isArray( source[ prop ] ) ) {
            target[ prop ] = source[ prop ].slice();
          } else {
            target[ prop ] = source[ prop ];
          }
        } else {
          target[ prop ] = source[ prop ];
        }
      }
    }

    return target;
  }

} ) );
