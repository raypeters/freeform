var FormaSupplyCoCart = new Freeform( 'FormaSupplyCoCart', {
  action: buildCrateJoySubmitButton,
  transformer: function( options ) {
    return FormaSupplyCoVariantMap[ options.tier + '-' + options.frequency ];
  },
  flow: [
    {
      key: 'product',
      flow: [
        { key: 'tier' },
        { key: 'frequency' }
      ],
      resolve: function( options ) {
        return ( options.tier + '-' + options.frequency );
      }
    },
    { key: 'tone' },
    { key: 'size' },
    {
      key: 'terms',
      require: function( options ) {
        return ( options.frequency === 'monthly' );
      }
    }
  ]
} );

var submitButton = FormaSupplyCoCart.element.querySelector( '.freeform__submit__button' );
submitButton.onclick = function() {
  FormaSupplyCoCart.submit();
};

/**
 * buildCrateJoySubmitButton
 *
 * CrateJoy's checkout process is simple and awesome. As you select
 * various options, it appends your selections to the URL, as such:
 *
 * 1. https://formasupplyco.cratejoy.com/subscribe
 * 2. https://formasupplyco.cratejoy.com/subscribe/687936703_essentials-monthly
 * 3. https://formasupplyco.cratejoy.com/subscribe/687936703_essentials-monthly/687936789_light
 *
 * In order to achieve the same thing on a single page, we only need
 * to programmatically build a URL the same way.
 *
 */

function buildCrateJoySubmitButton( options ) {
  var destination, submitButton;

  destination = 'https://formasupplyco.cratejoy.com/subscribe/';

  destination += [
    options.product,
    options.tone,
    options.size
  ].join( '/' );

  if ( options.terms ) {
    destination += ( '/' + options.terms );
  }

  FormaSupplyCoCart.element.querySelector( '.freeform__submit__button' ).scrollIntoView( true );

  return destination;
}

FormaSupplyCoVariantMap = {
  'basics-monthly': {
    product: '687925702_basics-monthly',
    tone: [
      '687925790_dark',
      '687925791_light',
      '687925792_mix'
    ],
    size: [
      '687925793_small',
      '687925794_medium',
      '687925795_large',
      '687925796_x-large',
      '687925797_xx-large'
    ],
    terms: [
      '687925703_month-to-month',
      '687925705_3-month-prepay',
      '687925706_6-month-prepay',
      '687925707_12-month-prepay'
    ]
  },

  'essentials-monthly': {
    product: '687936703_essentials-monthly',
    tone: [
      '687936788_dark',
      '687936789_light',
      '687936790_mix'
    ],
    size: [
      '687936791_small',
      '687936792_medium',
      '687936793_large',
      '687936794_x-large',
      '687936795_xx-large'
    ],
    terms: [
      '687936698_month-to-month',
      '687936699_3-month-prepay',
      '687936700_6-month-prepay',
      '687936701_12-month-prepay'
    ]
  },

  'select-monthly': {
    product: '687945029_select-monthly',
    tone: [
      '687945058_dark',
      '687945059_light',
      '687945060_mix'
    ],
    size: [
      '687945061_small',
      '687945062_medium',
      '687945063_large',
      '687945064_x-large',
      '687945065_xx-large'
    ],
    terms: [
      '687945030_month-to-month',
      '687945031_3-month-prepay',
      '687945032_6-month-prepay',
      '687945033_12-month-prepay'
    ]
  },

  'basics-bi-monthly': {
    product: '687928671_basics-bi-monthly',
    tone: [
      '687928761_dark',
      '687928762_light',
      '687928763_mix'
    ],
    size: [
      '687928764_small',
      '687928765_medium',
      '687928766_large',
      '687928767_x-large',
      '687928768_xx-large'
    ]
  },

  'essentials-bi-monthly': {
    product: '687938638_essentials-bi-monthly',
    tone: [
      '687938665_dark',
      '687938666_light',
      '687938667_mix'
    ],
    size: [
      '687938668_small',
      '687938669_medium',
      '687938670_large',
      '687938671_x-large',
      '687938672_xx-large'
    ]
  },

  'select-bi-monthly': {
    product: '687946878_select-bi-monthly',
    tone: [
      '687946964_dark',
      '687946965_light',
      '687946966_mix'
    ],
    size: [
      '687946967_small',
      '687946968_medium',
      '687946969_large',
      '687946970_x-large',
      '687946971_xx-large'
    ]
  },


  'basics-quarterly': {
    product: '687935353_basics-quarterly',
    tone: [
      '687935377_dark',
      '687935379_light',
      '687935380_mix'
    ],
    size: [
      '687935381_small',
      '687935382_medium',
      '687935383_large',
      '687935384_x-large',
      '687935385_xx-large'
    ]
  },

  'essentials-quarterly': {
    product: '687941118_essentials-quarterly',
    tone: [
      '687941206_dark',
      '687941207_light',
      '687941208_mix'
    ],
    size: [
      '687941209_small',
      '687941210_medium',
      '687941211_large',
      '687941212_x-large',
      '687941213_xx-large'
    ]
  },

  'select-quarterly': {
    product: '687948079_select-quarterly',
    tone: [
      '687948172_dark',
      '687948173_light',
      '687948174_mix'
    ],
    size: [
      '687948175_small',
      '687948176_medium',
      '687948177_large',
      '687948178_x-large',
      '687948179_xx-large'
    ]
  }
};
